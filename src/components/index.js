import Vue from "vue";
import FiberposUploader from "./FiberposUploader";

const Components = {
    FiberposUploader
};

Object.keys(Components).forEach(name => {
    Vue.component(name, Components[name]);
});

export default Components;