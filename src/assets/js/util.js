import _ from "underscore";
import _ResizeObserver from 'resize-observer-polyfill';

function _buildFormData(formData, data, parentKey = '') {
    if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File) && !(data instanceof Blob)) {
        _.each(data, function(value, key) {
            _buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
        });

        return;
    }

    const value = data === null ? '' : data;

    formData.append(parentKey, value);
}

class Listener {
    constructor() {
        this.elements = [];
    }

    addElement(element) {
        this.elements.push(element);
    }
}

(function () {
    if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) {
        params = params || {bubbles: false, cancelable: false, detail: undefined};
        let evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();

const windowScrollListener = new class ScrollListener extends Listener {
    constructor() {
        super();
        let _self = this;

        window.addEventListener('scroll', function () {
            for (let index in _self.elements) {
                let element = _self.elements[index];

                if (element.elem.getBoundingClientRect().top < window.innerHeight) {
                    element.callback();
                    // remove the element in the list to prevent calling the callback again
                    _self.elements.splice(index, 1);
                }
            }
        })
    }
};

const windowResizeListener = new class ResizeListener extends Listener {
    constructor() {
        super();
        let _self = this;

        let throttle = function (type, name, obj) {
            obj = obj || window;
            let running = false;
            let func = function () {
                if (running) {
                    return;
                }
                running = true;
                requestAnimationFrame(function () {
                    obj.dispatchEvent(new CustomEvent(name));
                    running = false;
                });
            };
            obj.addEventListener(type, func);
        };

        /* init - you can init any event */
        throttle("resize", "optimizedResize");

        window.addEventListener("optimizedResize", () => {
            for (let index in _self.elements) {
                let element = _self.elements[index];

                element.callback();
            }
        });
    }
};


/**
 * Get the first item that pass the test
 * by second argument function
 *
 * @param {Array} list
 * @param {Function} f
 * @return {*}
 */
export function find (list, f) {
  return list.filter(f)[0]
}

/**
 * Deep copy the given object considering circular structure.
 * This function caches all nested objects and its copies.
 * If it detects circular structure, use cached copy to avoid infinite loop.
 *
 * @param {*} obj
 * @param {Array<Object>} cache
 * @return {*}
 */
export function deepCopy (obj, cache = []) {
  // just return if obj is immutable value
  if (obj === null || typeof obj !== 'object') {
    return obj
  }

  // if obj is hit, it is in circular structure
  const hit = find(cache, c => c.original === obj);
  if (hit) {
    return hit.copy
  }

  const copy = Array.isArray(obj) ? [] : {};
  // put the copy into cache at first
  // because we want to refer it in recursive deepCopy
  cache.push({
    original: obj,
    copy
  });

  Object.keys(obj).forEach(key => {
    copy[key] = deepCopy(obj[key], cache)
  });

  return copy;
}

/**
 * forEach for object
 */
export function forEachValue (obj, fn) {
  Object.keys(obj).forEach(key => fn(obj[key], key))
}

export function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

export function isPromise (val) {
  return val && typeof val.then === 'function'
}

export function assert (condition, msg) {
  if (!condition) throw new Error(`[vuex] ${msg}`)
}

export function sliceText(text, maxLength = 80, suffix = '...') {
    if (text.length <= maxLength) {
        return text;
    }
    const options = {
        // By default we add an ellipsis at the end
        suffix: true,
        suffixString: suffix,
        // By default we preserve word boundaries
        preserveWordBoundaries: true,
        wordSeparator: " "
    };

    // Compute suffix to use (eventually add an ellipsis)
    let _suffix = "";
    if (text.length > maxLength && options.suffix) {
        _suffix = options.suffixString;
    }

    // Compute the index at which we have to cut the text
    const maxTextLength = maxLength - _suffix.length;

    let cutIndex = maxTextLength;
    if (options.preserveWordBoundaries) {
        // We use +1 because the extra char is either a space or will be cut anyway
        // This permits to avoid removing an extra word when there's a space at the maxTextLength index
        const lastWordSeparatorIndex = text.lastIndexOf(options.wordSeparator, maxTextLength + 1);
        // We include 0 because if have a "very long first word" (size > maxLength), we still don't want to cut it
        // But just display "...". But in this case the user should probably use preserveWordBoundaries:false...
        cutIndex = lastWordSeparatorIndex > 0 ? lastWordSeparatorIndex : maxTextLength;
    }

    return text.substr(0, cutIndex) + _suffix;
}

export function addImageProcess(imageSrc){
    return new Promise((resolve, reject) => {
        let img = new Image();
        img.onload = () => resolve(img.src);
        img.onerror = reject;
        img.src = imageSrc;
    })
}

export function uniqueId() {
    return 'yxxxx'.replace(/[xy]/g, function (c) {
        let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

export function createFormData(data) {
    let formData = new FormData();

    _buildFormData(formData, data);

    return formData;
}

/**
 * @param {Number} number
 * @param {Number} [minimumTransform=10000]
 * @param {Number} [decimalPlaces=2]
 * @returns {*}
 */
export function getAbbreviatedNumber(number, minimumTransform = 10000, decimalPlaces = 2) {
    if (number < minimumTransform) {
        return numberWithComma(number);
    }

    decimalPlaces = Math.pow(10, decimalPlaces);

    // Enumerate number abbreviations
    const abbrev = ["K", "M", "B", "T"];

    // Go through the array backwards, so we do the largest first
    for (let i = abbrev.length - 1; i >= 0; i--) {

        // Convert array index to "1000", "1000000", etc
        const size = Math.pow(10, (i + 1) * 3);

        // If the number is bigger or equal do the abbreviation
        if (size <= number) {
            // Here, we multiply by decPlaces, round, and then divide by decPlaces.
            // This gives us nice rounding to a particular decimal place.
            number = Math.round(number * decimalPlaces / size) / decimalPlaces;

            // Handle special case where we round up to the next abbreviation
            if ((number == 1000) && (i < abbrev.length - 1)) {
                number = 1;
                i++;
            }

            // Add the letter for the abbreviation
            number += abbrev[i];

            // We are done... stop
            break;
        }
    }

    return number;
}

export function numberWithComma(number) {
    // 9153 will convert to 9,153
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export function getCsrfToken() {
    return document.querySelector('meta[name="csrf-token"]').getAttribute('content');
}

// TODO: image ratio is 1x1 need to make it dynamic but for now this is accepted
export const responsiveImageObserver = (function () {
    const func = (entries, observer) => {
        for (const entry of entries) {
            const {width} = entry.contentRect;
            const image = entry.target.querySelector('img');

            entry.target.style.height = `${width}px`;

            image.style.maxWidth = `${width}px`;
            image.style.maxHeight = `${width}px`;
        }
    };

    if (!window.ResizeObserver) {
        // use plugin since browser do not support current class
        return new _ResizeObserver(func);
    }

    return new window.ResizeObserver(func);
})();

export function addToLazyLoadUponScroll(element) {
    windowScrollListener.addElement(element);
}

export function addEventUponResize(element) {
    windowResizeListener.addElement(element);
}

export function dataURLtoBlob(dataurl) {
    const arr = dataurl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }

    return new Blob([u8arr], {type: mime});
}

export function blobToDataURL(blob, callback) {
    const a = new FileReader();

    a.onload = function (e) {
        callback(e.target.result);
    };

    a.readAsDataURL(blob);
}

