import api from '../index'

export function getMediaLibrary(page, searchName, sortBy, successHandler, errorHandler = null) {
    let url = `/media/get-upload-file/${page}/${sortBy}`;
    if (searchName) {
        url += `/${searchName}`;
    }
    api.get(url, successHandler, errorHandler);
}