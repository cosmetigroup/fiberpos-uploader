// NOTE: DEPRECATED TYPES
export const FILE_TYPE_FILES = 'files';
export const FILE_TYPE_AVATAR = 'avatar';

// NOTE: UPLOAD TYPES
export const FILE_SINGLE = 'single';
export const FILE_MULTIPLE = 'multiple';

export const FILE_TYPE_IMAGE = 'image';
export const FILE_TYPE_DOCUMENT = 'document';

// NOTE: DEFAULT ALLOWED EXTENSIONS
export const FILE_DEFAULT_ALLOWED_EXTENSIONS = [
    'image/png',
    'image/jpeg',
    'image/gif',
    'image/bmp',
    'image/x-icon',
    'application/pdf',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.oasis.opendocument.text',
    'application/zip',
    'application/x-compressed',
    'application/x-gzip',
    'application/x-rar-compressed',
    'application/x-zip-compressed',
    'application/x-7z-compressed',
    'application/x-bzip',
    'application/x-bzip2',
    'application/epub+zip',
    'application/vnd.rar',
    'audio/mp3',
    'audio/mpeg',
    'audio/ogg',
    'audio/x-wav',
    'audio/3gpp',
    'audio/3gpp2',
    'video/mp4',
    'video/quicktime',
    'video/x-msvideo',
    'video/mpeg',
    'video/ogg',
    'video/3gpp',
    'video/3gpp2',
    'text/plain'
];