import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import './components';

Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

Array.from(document.querySelectorAll('.fiberpos-uploader')).forEach(function (el) {
    new Vue({
        el
    });
});