import * as BLOB_IMAGES from "../assets/js/utility/blob-images";
import * as UPLOAD_TYPES from "../assets/js/utility/upload-types";

export default {
    data() {
        return {
            icon: {
                pdf: 'fa fa-file-pdf text-danger',
                word: 'fa fa-file-alt text-primary',
                excel: 'fa fa-file-csv text-success',
                powerpoint: 'fa fa-file-powerpoint text-danger',
                audio: 'fa fa-file-audio',
                video: 'fa fa-file-video',
                archive: 'fa fa-file-archive',
                default: 'far fa-file-alt'
            },
            duplicateTypes: ['mpeg', '3gpp', 'ogg', '3gpp2']
        }
    },
    methods: {
        showToastSuccess(title, message) {
            if (common !== undefined) {
                common.showToastSuccess(title, message);
                return;
            }
            // Toaster Fallback
            alert(`${title}: ${message}`)
        },
        showToastError(title, message) {
            if (common !== undefined) {
                common.showToastError(title, message);
                return;
            }
            // Toaster Fallback
            alert(`${title}: ${message}`)
        },
        validateFile(file, allowedMimeTypes) {
            if (!allowedMimeTypes.length) {
                allowedMimeTypes = UPLOAD_TYPES.FILE_DEFAULT_ALLOWED_EXTENSIONS
            }

            if (!allowedMimeTypes.includes(file.type)) {
                return 'Cannot use selected file.';
            }

            if (file.size > 5242880) {
                return 'Cannot upload file larger than 5MB.';
            }

            return '';
        },
        isFileTypeImage(file) {
            return file.type.split('/')[0] === 'image';
        },
        isFileTypeGeneral(file) {
            return !this.isFileTypeImage(file);
        },
        isWhichFile(file) {
            let fileType = file.type.split('/')[1];
            let icon;
            switch (fileType) {
                case "pdf":
                    icon = this.icon.pdf;
                    break;

                case "doc":
                case "docx":
                case "msword":
                case "vnd.openxmlformats-officedocument.wordprocessingml.document":
                    icon = this.icon.word;
                    break;

                case "xls":
                case "xlsx":
                case "vnd.ms-excel":
                case "vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    icon = this.icon.excel;
                    break;

                case "ppt":
                case "pptx":
                case "pps":
                case "ppsx":
                case "vnd.ms-powerpoint":
                case "vnd.openxmlformats-officedocument.presentationml.presentation":
                    icon = this.icon.powerpoint;
                    break;

                case "mp3":
                case "m4a":
                case "wav":
                    icon = this.icon.audio;
                    break;

                case "mp4":
                case "mov":
                case "wmv":
                case "avi":
                case "mpg":
                case "ogv":
                case "quicktime":
                case "x-msvideo":
                    icon = this.icon.video;
                    break;

                case "zip":
                case 'x-compressed':
                case 'x-gzip':
                case 'x-rar-compressed':
                case 'x-zip-compressed':
                case 'x-7z-compressed':
                case 'x-bzip':
                case 'x-bzip2':
                case 'epub+zip':
                case 'vnd.rar':
                    icon = this.icon.archive;
                    break;

                default:
                    icon = this.icon.default;
            }

            if (this.duplicateTypes.includes(fileType)) {
                if (file.type.split('/')[0] === 'audio') {
                    icon = this.icon.audio;
                    return icon;
                }
                if (file.type.split('/')[0] === 'video') {
                    icon = this.icon.video;
                    return icon;
                }
            }

            return icon;
        }
    }
}